<?php

namespace Tests\Functional;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class ApiTest extends BaseTestCase
{
    /**
     * Test Connection Successfully
    */

    public function testConnectionSuccessfully()
    {
        $correctResponse = file_get_contents(__DIR__ . '/../Responses/listAlbums.json');

        // Create a mock and queue two responses.
        $response = new MockHandler([
            new Response(200, [], $correctResponse),
        ]);

        $handler = HandlerStack::create($response);
        $client = new Client(['handler' => $handler]);

        // The request is intercepted with the response.
        $data = $client->request('GET', '/albums?q=silvester');
        $statusCode = $data->getStatusCode();

        $this->assertEquals(200, $statusCode);
        $this->assertEquals($correctResponse, $data->getBody()->getContents());
    }

    /**
     * Test Connection Failed
     */

    public function testConnectionFailed()
    {
        $failedAnswer = 'Error connect with Spotify';

        // Create a mock and queue two responses.
        $response = new MockHandler([
            new Response(200, [], $failedAnswer),
            new Response(500, [], $failedAnswer),
        ]);

        $handler = HandlerStack::create($response);
        $client = new Client(['handler' => $handler]);

        // The request is intercepted with the response.
        $data = $client->request('GET', '/albums?q=');
        $statusCode = $data->getStatusCode();

        $this->assertEquals(200, $statusCode);
        $this->assertEquals($failedAnswer, $data->getBody()->getContents());
    }

    /**
     * Test Error to Token Generate
     */

    public function testErrorToTokenGenerate()
    {
        $failedToken = 'Error to Token Generate';

        // Create a mock and queue two responses.
        $response = new MockHandler([
            new Response(200, [], $failedToken),
        ]);

        $handler = HandlerStack::create($response);
        $client = new Client(['handler' => $handler]);

        // The request is intercepted with the response.
        $data = $client->request('GET', '/albums?q=silvester');
        $statusCode = $data->getStatusCode();

        $this->assertEquals(200, $statusCode);
        $this->assertEquals($failedToken, $data->getBody()->getContents());
    }

}