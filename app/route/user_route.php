<?php

$containers = $app->getContainer();

$app->group('/api/v1', function () use ($app, $containers) {
    $this->get('/albums',
        'SpotifyController:index'
    );
});

