<?php
/**
 * HTTP Client Container.
 *
 * @author Pablo Anello <panello@aivo.co>
 */
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;

$logger = $app->getContainer()->get('logger');

/**
 * @return Client
 */
$container["httpClient"] = function () use ($logger) {
    $logHandler = HandlerStack::create();
    $logHandler->push(
        Middleware::log(
            $logger,
            new MessageFormatter(MessageFormatter::DEBUG)
        )
    );

    $guzzle = new Client([
        'handler' => $logHandler,
    ]);

    return $guzzle;
};
