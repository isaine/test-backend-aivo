<?php

/**
 * Created by PhpStorm.
 * User: iduque
 * Date: 15/06/19
 */

namespace App\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Client;

class SpotifyController
{
    /**
     * constants
     */
    const CLIENT_ID = 'a9f42b16359f4fb09dcff4bf75c24c43';
    const CLIENT_SECRET = '4f5e97c3e7a34990b6411d16d680a834';

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @throws
     * If there is a connection problem with spotify
     */

    public function index(Request $request, Response $response, $args = [])
    {
        $artist = $request->getParams();

        $url = 'https://api.spotify.com/v1/search?q='.$artist['q'].'&type=album';
        $client = new Client();
        $token = $this->getToken();

        if (! empty($token)) {
            try {
                $options = [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer '.$token,
                    ],
                ];

                $res = $client->request('GET', $url, $options);
                $data = json_decode($res->getBody(), true);

                foreach ($data['albums']['items'] as $albums) {
                    $dataAlbums[] = [
                        'name' => $albums['name'],
                        'released' => $albums['release_date'],
                        'tracks' => $albums['total_tracks'],
                        'cover' => $albums['images'],
                    ];
                }
                echo json_encode($dataAlbums);
            } catch (BadResponseException $e) {
                echo 'Error connect with Spotify';
            }
        } else {
            echo 'Error to Token Generate';
        }
    }

    /**
     *  In this function we generate the token for authorization spotify using API Spotify
     *
     *  return string;
     */

    private function getToken()
    {
        $url = 'https://accounts.spotify.com/api/token';

        $client = new Client();
        $credentials = base64_encode(SpotifyController::CLIENT_ID.':'.SpotifyController::CLIENT_SECRET);
        $oauth = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Basic '.$credentials,
            ],
            'form_params' => [
                'grant_type' => 'client_credentials',
            ],
        ];

        try {
            $res = $client->request('POST', $url, $oauth);
            $token = json_decode($res->getBody(), true);
        } catch (BadResponseException $e) {
            echo 'Error with generate Token in Spotify';
        }

        return $token['access_token'];
    }
}
